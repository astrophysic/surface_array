#include "Array.hpp"
#include <cmath>
#include <fstream>
#include <iomanip>

#include "TH2F.h"
#include "TFile.h"


Array::Array()
{
   this->distance=1500;
   GenerateCore(); 
   GenerateGrid(4);
   Generator.SetSeed(0);
   srand (time(NULL));
}


Array::Array(double distance, size_t n_crown, std::string file_name)
{
    TFile* map_file = new TFile(file_name.c_str());
  muon_map =  (TH2F*) map_file->Get("Muon2D");
  muon_map->SetDirectory(0);
  map_file->Close();

   this->distance=distance;
   GenerateCore(); 
   GenerateGrid(n_crown);
   Generator.SetSeed(1);
   srand (time(NULL));
}

Array::Array(std::string dir, std::string name)
{
    std::ifstream in_file((dir+name).c_str(),std::fstream::in);
    if (in_file.is_open())
    {
        double pos_x, pos_y, signal, time;

        in_file >> this->core_x >> this->core_y;
        while(!in_file.eof())
        {
            in_file >> pos_x >> pos_y >> time >> signal;
            detectors.push_back(new Detector(pos_x,pos_y));
            detectors.back()->SetSignal(signal);
            detectors.back()->ChangeTime(time);
            
        }
        this->distance=1500;
    }
}


Array::Array(std::string file_name)
{

  TFile* map_file = new TFile(file_name.c_str());
  muon_map =  (TH2F*) map_file->Get("Muon2D");
  muon_map->SetDirectory(0);
  map_file->Close();

   this->distance=1500;
   GenerateCore(); 
   GenerateGrid(4);
   Generator.SetSeed(1);
   srand (time(NULL));
}

size_t Array::GetSize()
{
    return detectors.size();
}

void Array::Erase()
{
    for (auto&& detector : detectors)
    {
        detector->ClearTime();
    }
    
}

Array::~Array()
{
    for (auto&& detector : detectors)
    {
        delete(detector);
    }
    detectors.clear();
}

void Array::GenerateGrid(size_t n_crown)
{
    double cos_d = distance * cos(M_PI*60/180);
    double sin_d = distance * sin(M_PI*60/180);

    detectors.push_back( new Detector( 0, 0) ); 

    for(size_t i = 1; i <= n_crown; i++)
    {
        double x = i*distance;
        double y = 0;

        for(size_t j = 1; j < i+1; j++)
        {
            x = x - cos_d;
            y = y + sin_d;
            detectors.push_back( new Detector( x, y) );        
        }
        for(size_t j = 1; j < i+1; j++)
        {
            x = x - distance;
            y = y;
            detectors.push_back( new Detector( x, y) );    
        }
        for(size_t j = 1; j < i+1; j++)
        {
            x = x - cos_d;
            y = y - sin_d;
            detectors.push_back( new Detector( x, y) );    
        }
        for(size_t j = 1; j < i+1; j++)
        {
            x = x + cos_d;
            y = y - sin_d;
            detectors.push_back( new Detector( x, y) );    
        }
        for(size_t j = 1; j < i+1; j++)
        {
            x = x + distance;
            y = y;
            detectors.push_back( new Detector( x, y) );    
        }
        for(size_t j = 1; j < i+1; j++)
        {
            x = x + cos_d;
            y = y + sin_d;
            detectors.push_back( new Detector( x, y) );    
        }        
    } 
}



void Array::GenerateCore()
{
    
    double x, y, r, u, tan;
    while (true)
    {
        x = (3000.0*static_cast <double> (rand()) / static_cast <double> (RAND_MAX)) - 1500.0;
        y = (3000.0*static_cast <double> (rand()) / static_cast <double> (RAND_MAX)) - 1500.0;

        tan = atan(y/x);
        tan = tan - (floor(tan*3.0/M_PI))*M_PI/3;

        r = sqrt(pow(x,2)+pow(y,2));
        u = 1500*(cos(M_PI/6)/cos(M_PI/6-tan));
        if (r < u)
        {
            break;
        }

    }
    this->core_x=x;
    this->core_y=y;
}


void Array::Print(std::string save_path)
{
    std::ofstream out_file(save_path.c_str(),std::fstream::out);

    out_file << std::fixed;
    out_file << std::setprecision(6);
    out_file << core_x << " " << core_y << "\n";
    
    for(auto detector : detectors)
    {
        out_file << std::setw(15) << std::setprecision(6) << detector->GetX() << " " <<
                    std::setw(15) << std::setprecision(6) << detector->GetY() << " " <<
                    std::setw(15) << std::setprecision(6) << detector->GetTime() <<
                    std::setw(15) << std::setprecision(6) << detector->GetSignal() << '\n';

    }    
    out_file.close();
}

void Array::Crossed(ParticleArray& particles)
{
    for(auto particle : particles)
    {
        for (auto detector : detectors)
        {
            detector->Crossed(particle,core_x,core_y);
        }
    }
}

void Array::GenerateTimeCurvature(double theta, double phi){
  double u = sin(theta*M_PI/180)*cos(phi*M_PI/180);
  double v = sin(theta*M_PI/180)*sin(phi*M_PI/180);
  double T0 = 740000;
  double R = 10000;

  for (auto detector : detectors)
  {
      double pos_x = detector->GetX()-core_x;
      double pos_y = detector->GetY()-core_y;

      double rp = u*pos_x + v*pos_y;
      double rho = sqrt(pos_x*pos_x+pos_y*pos_y);
      double delta = (rho*rho - rp*rp);

      double time = T0 + rp/0.3 + delta/(2*R*0.3);
      double signal = detector->GetSignal();
      double dt;

    if (signal > 0){
        double tau = 0.97*pow((2*0.5*sqrt(delta)/signal),2)*((signal-1)/(signal+1));
        dt = Generator.Exp(sqrt(tau));
        detector->ChangeTime(time+dt);
    } 
  }
}

void Array::GenerateSignal( ){
  
  for(auto detector : detectors)
  {
      double pos_x = detector->GetX() - core_x;
      double pos_y = detector->GetY() - core_y;
      double signal = detector->GetArea()*muon_map->Interpolate(pos_x,pos_y);
      double ds = Generator.Poisson(signal);
      detector->SetSignal(ds);
  }

}

void Array::GenerateTimePlane(double theta, double phi){
  double u = sin(theta*M_PI/180)*cos(phi*M_PI/180);
  double v = sin(theta*M_PI/180)*sin(phi*M_PI/180);
  double T0 = 740000;
  double Rc = 10000;

    for (auto detector : detectors)
    {
        double PosX = detector->GetX() - core_x;
        double PosY = detector->GetY() - core_y;
        double PosR = sqrt(pow(PosX,2)+pow(PosY,2));
        double Time = T0 + (u*(PosX-core_x)+v*(PosY-core_y))/0.3;// + (PosR*PosR)/(2*Rc*0.3);
        double dt = Generator.Gaus(Time,25); //Error
        detector->ChangeTime(dt);
    }
}

void Array::GetData(double* x, double *y, double *z, double *e){
  
    for (int i = 0; i < detectors.size(); i++) {
    x[i]=detectors.at(i)->GetX()-core_x;
    y[i]=detectors.at(i)->GetY()-core_y;
    z[i]=detectors.at(i)->GetSignal();
    e[i]=sqrt(z[i]);
  }
}

void Array::GetData(double* x, double *y, double *z, double *e, double* t){
  
    for (int i = 0; i < detectors.size(); i++) {
    x[i]=detectors.at(i)->GetX()-core_x;
    y[i]=detectors.at(i)->GetY()-core_y;
    z[i]=detectors.at(i)->GetSignal();
    e[i]=sqrt(z[i]);
    t[i]=detectors.at(i)->GetTime();
  }
}