#include "Detector.hpp"
#include <cmath>

Detector::Detector(double x, double y)
{
    this->radius = 1.8;
    this->position[0] = x;
    this->position[1] = y;
}

Detector::~Detector()
{

}

double Detector::GetSignal(){
    return this->trigger_times.size(); 
}

double Detector::GetTime()
{
    if (trigger_times.size()>0){
        double min_time = this->trigger_times.at(0);
        
        for(auto time : trigger_times)
        {
            if (time < min_time)
            {
                min_time = time;
            }
        }
        return min_time;
    }
    return 0;
}

double Detector::GetX()
{
    return this->position[0];
}

double Detector::GetY()
{
    return this->position[1];
}

void Detector::Crossed(CorsikaParticle* particle, double core_x, double core_y)
{
    Particle teste;

    _4Vector _position = particle->GetPosition();

    double pos_x = particle->GetPosition()[1];
    double pos_y = particle->GetPosition()[2];
    double x = this->position[0]-pos_x-core_x;
    double y = this->position[1]-pos_y-core_y;

    double dist = sqrt(pow(x,2) + pow(y,2));

    if (dist < radius) {
        trigger_times.push_back(particle->GetPosition()[0]);
    }
}

void Detector::ChangeTime(double time)
{
    for (auto&& trigger_time :  trigger_times)
    {
        trigger_time = time;
    }
}

double Detector::GetArea()
{
    return M_PI*radius*radius;
}

void Detector::SetSignal(double signal)
{
    for(size_t i = 0; i < signal; i++)
    {
        trigger_times.push_back(0);     
    } 
}

void Detector::ClearTime()
{
    trigger_times.clear();
}