SRCDIR := src
OBJDIR := lib
INCDIR := inc

SRCS := $(wildcard $(SRCDIR)/*cpp)
OBJS := $(addprefix $(OBJDIR)/, $(notdir $(SRCS:.cpp=.o)))

INC := -Iinc -I../corsika/inc
LIB := -L../corsika/lib
CFLAGS := -g -std=c++17
CLIBS := -lCorsika

CC := g++

all:$(OBJDIR)/libArray.a

$(OBJDIR)/libArray.a: $(OBJS) 
	ar rvs -o $@ $^ 

$(OBJDIR)/%.o: $(SRCDIR)/%.cpp | $(OBJDIR)/
	$(CC) $(CFLAGS) -c $< -o $@ $(INC) `$(ROOTSYS)/bin/root-config --cflags --glibs`

$(OBJDIR)/: 
	mkdir -p $@

clean:
	rm -rf lib/*.o
	rm -rf lib/*.a
