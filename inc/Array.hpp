#ifndef _ARRAY_HPP_
#define _ARRAY_HPP_

#include <iostream>
#include <vector>
#include <string>

#include "Detector.hpp"
#include "CorsikaParticle.hpp"
#include "TRandom.h"
#include "TH2F.h"

typedef std::vector<Detector*> DetectorArray;

using namespace std;

class Array {
    DetectorArray detectors;
    double distance;
    double core_x;
    double core_y;
    void GenerateGrid(size_t n_crown);
    TRandom Generator;
    TH2F* muon_map;
public:
    Array();
    Array(string file_name);
    Array(string dir, string name);
    Array(double distance, size_t n_crown, string file_name);
    ~Array();
    void GenerateCore();
    void Crossed(ParticleArray& particles);
    void Print(string save_path);
    void GenerateSignal();
	void GenerateTimePlane(double theta, double phi);
    void GenerateTimeCurvature(double theta, double phi);
    size_t GetSize();
    void GetData(double* x, double *y, double *z, double *e);
    void GetData(double* x, double *y, double *z, double *e, double* t);
    void Erase();
};

#endif /* _ARRAY_HPP_ */