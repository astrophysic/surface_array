#ifndef _DETECTOR_HPP_
#define _DETECTOR_HPP_

#include <iostream>
#include <vector>

#include "CorsikaParticle.hpp"


typedef std::vector<double> TimeArray;

class Detector {
    double position[3];
    TimeArray trigger_times;
    double radius;
public:
    Detector(double x, double y);
    ~Detector();
    double GetX();
    double GetY();
    double GetTime();
    double GetSignal();
    void Crossed(CorsikaParticle* particle, double core_x, double core_y);
    void ChangeTime(double time);
    double GetArea();
    void SetSignal(double signal);
    void ClearTime();
};

#endif /* _DETECTOR_HPP_ */